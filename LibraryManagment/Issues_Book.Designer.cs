﻿namespace LibraryManagment
{
    partial class Issues_Book
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.e_textBox = new System.Windows.Forms.TextBox();
            this.n_textBox = new System.Windows.Forms.TextBox();
            this.d_textBox = new System.Windows.Forms.TextBox();
            this.s_textBox = new System.Windows.Forms.TextBox();
            this.c_textBox = new System.Windows.Forms.TextBox();
            this.em_textBox = new System.Windows.Forms.TextBox();
            this.bn_textBox = new System.Windows.Forms.TextBox();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.s_button = new System.Windows.Forms.Button();
            this.i_button = new System.Windows.Forms.Button();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.listBox1);
            this.panel1.Controls.Add(this.i_button);
            this.panel1.Controls.Add(this.s_button);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.dateTimePicker1);
            this.panel1.Controls.Add(this.bn_textBox);
            this.panel1.Controls.Add(this.em_textBox);
            this.panel1.Controls.Add(this.c_textBox);
            this.panel1.Controls.Add(this.s_textBox);
            this.panel1.Controls.Add(this.d_textBox);
            this.panel1.Controls.Add(this.n_textBox);
            this.panel1.Controls.Add(this.e_textBox);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(42, 27);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(841, 440);
            this.panel1.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(36, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(101, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Enter Enrollment No";
            // 
            // e_textBox
            // 
            this.e_textBox.Location = new System.Drawing.Point(25, 49);
            this.e_textBox.Name = "e_textBox";
            this.e_textBox.Size = new System.Drawing.Size(132, 20);
            this.e_textBox.TabIndex = 1;
            this.e_textBox.KeyUp += new System.Windows.Forms.KeyEventHandler(this.e_textBox_KeyUp);
            // 
            // n_textBox
            // 
            this.n_textBox.Location = new System.Drawing.Point(516, 49);
            this.n_textBox.Name = "n_textBox";
            this.n_textBox.Size = new System.Drawing.Size(248, 20);
            this.n_textBox.TabIndex = 2;
            // 
            // d_textBox
            // 
            this.d_textBox.Location = new System.Drawing.Point(516, 87);
            this.d_textBox.Name = "d_textBox";
            this.d_textBox.Size = new System.Drawing.Size(248, 20);
            this.d_textBox.TabIndex = 3;
            // 
            // s_textBox
            // 
            this.s_textBox.Location = new System.Drawing.Point(516, 127);
            this.s_textBox.Name = "s_textBox";
            this.s_textBox.Size = new System.Drawing.Size(248, 20);
            this.s_textBox.TabIndex = 4;
            // 
            // c_textBox
            // 
            this.c_textBox.Location = new System.Drawing.Point(516, 170);
            this.c_textBox.Name = "c_textBox";
            this.c_textBox.Size = new System.Drawing.Size(248, 20);
            this.c_textBox.TabIndex = 5;
            // 
            // em_textBox
            // 
            this.em_textBox.Location = new System.Drawing.Point(516, 208);
            this.em_textBox.Name = "em_textBox";
            this.em_textBox.Size = new System.Drawing.Size(248, 20);
            this.em_textBox.TabIndex = 6;
            // 
            // bn_textBox
            // 
            this.bn_textBox.Location = new System.Drawing.Point(516, 301);
            this.bn_textBox.Name = "bn_textBox";
            this.bn_textBox.Size = new System.Drawing.Size(248, 20);
            this.bn_textBox.TabIndex = 7;
            this.bn_textBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.bn_textBox_KeyDown);
            this.bn_textBox.KeyUp += new System.Windows.Forms.KeyEventHandler(this.bn_textBox_KeyUp);
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(516, 257);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(248, 20);
            this.dateTimePicker1.TabIndex = 8;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(416, 55);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(75, 13);
            this.label2.TabIndex = 9;
            this.label2.Text = "Student Name";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(416, 94);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(62, 13);
            this.label3.TabIndex = 10;
            this.label3.Text = "Department";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(419, 133);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 13);
            this.label4.TabIndex = 11;
            this.label4.Text = "Seamister";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(422, 176);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(44, 13);
            this.label5.TabIndex = 12;
            this.label5.Text = "Contact";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(425, 214);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(32, 13);
            this.label6.TabIndex = 13;
            this.label6.Text = "Email";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(425, 257);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(63, 13);
            this.label7.TabIndex = 14;
            this.label7.Text = "Issues Date";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(428, 307);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(63, 13);
            this.label8.TabIndex = 15;
            this.label8.Text = "Book Name";
            // 
            // s_button
            // 
            this.s_button.Location = new System.Drawing.Point(48, 84);
            this.s_button.Name = "s_button";
            this.s_button.Size = new System.Drawing.Size(75, 23);
            this.s_button.TabIndex = 16;
            this.s_button.Text = "Search";
            this.s_button.UseVisualStyleBackColor = true;
            this.s_button.Click += new System.EventHandler(this.s_button_Click);
            // 
            // i_button
            // 
            this.i_button.Location = new System.Drawing.Point(280, 393);
            this.i_button.Name = "i_button";
            this.i_button.Size = new System.Drawing.Size(75, 23);
            this.i_button.TabIndex = 17;
            this.i_button.Text = "Issue Book";
            this.i_button.UseVisualStyleBackColor = true;
            this.i_button.Click += new System.EventHandler(this.i_button_Click);
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Location = new System.Drawing.Point(516, 321);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(248, 95);
            this.listBox1.TabIndex = 18;
            this.listBox1.Visible = false;
            this.listBox1.MouseClick += new System.Windows.Forms.MouseEventHandler(this.listBox1_MouseClick);
            this.listBox1.SelectedIndexChanged += new System.EventHandler(this.listBox1_SelectedIndexChanged);
            this.listBox1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.listBox1_KeyDown);
            // 
            // Issues_Book
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(939, 493);
            this.Controls.Add(this.panel1);
            this.Name = "Issues_Book";
            this.Text = "Issues Book";
            this.Load += new System.EventHandler(this.Issues_Book_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button i_button;
        private System.Windows.Forms.Button s_button;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.TextBox bn_textBox;
        private System.Windows.Forms.TextBox em_textBox;
        private System.Windows.Forms.TextBox c_textBox;
        private System.Windows.Forms.TextBox s_textBox;
        private System.Windows.Forms.TextBox d_textBox;
        private System.Windows.Forms.TextBox n_textBox;
        private System.Windows.Forms.TextBox e_textBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ListBox listBox1;
    }
}