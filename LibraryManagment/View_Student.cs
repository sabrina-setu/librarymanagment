﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.IO;

namespace LibraryManagment
{
    public partial class View_Student : Form
    {
        SqlConnection con = new SqlConnection(@"Data Source=DESKTOP-ADPGDPU;Initial Catalog=library_managment;Integrated Security=True");
        string wanted_path;
        string pwd = Class1.GetRandomPassword(20);
        DialogResult result;
        public View_Student()
        {
            InitializeComponent();
        }

        private void View_Student_Load(object sender, EventArgs e)
        {
           
            if(con.State==ConnectionState.Open)
            {
                con.Close();
            }

            con.Open();

            fill_grid();
        }

        public void fill_grid()
        {
            dataGridView1.Columns.Clear();
            dataGridView1.Refresh();
            int i = 0;
            SqlCommand cmd = con.CreateCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "select *from S_Info";
            cmd.ExecuteNonQuery();
            DataTable dt = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            dataGridView1.DataSource = dt;

            Bitmap img;
            DataGridViewImageColumn imageCol = new DataGridViewImageColumn();
            imageCol.Width = 500;
            imageCol.HeaderText = "Student Image";
            imageCol.ImageLayout = DataGridViewImageCellLayout.Zoom;
            imageCol.Width = 100;
            dataGridView1.Columns.Add(imageCol);

            foreach (DataRow dr in dt.Rows)
            {
                img = new Bitmap(@"..\..\" + dr["S_Image"].ToString());
                dataGridView1.Rows[i].Cells[8].Value = img;
                dataGridView1.Rows[i].Height = 100;
                i++;
            }
        }

        private void S_textBoxKeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                dataGridView1.Columns.Clear();
                dataGridView1.Refresh();
                int i = 0;
                SqlCommand cmd = con.CreateCommand();
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "select *from S_Info where S_Name like ('%" + S_textBox.Text + "%')";
                cmd.ExecuteNonQuery();
                DataTable dt = new DataTable();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
                dataGridView1.DataSource = dt;

                Bitmap img;
                DataGridViewImageColumn imageCol = new DataGridViewImageColumn();
                imageCol.Width = 500;
                imageCol.HeaderText = "Student Image";
                imageCol.ImageLayout = DataGridViewImageCellLayout.Zoom;
                imageCol.Width = 100;
                dataGridView1.Columns.Add(imageCol);

                foreach (DataRow dr in dt.Rows)
                {
                    img = new Bitmap(@"..\..\" + dr["S_Image"].ToString());
                    dataGridView1.Rows[i].Cells[8].Value = img;
                    dataGridView1.Rows[i].Height = 100;
                    i++;
                }


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString());
            }
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int i;
            i = Convert.ToInt32(dataGridView1.SelectedCells[0].Value.ToString());
            //MessageBox.Show(i.ToString());
            try
            {
                
                SqlCommand cmd = con.CreateCommand();
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "select *from S_Info where Id=" + i + "";
                cmd.ExecuteNonQuery();
                DataTable dt = new DataTable();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
                foreach (DataRow dr in dt.Rows)
                {
                    s_text.Text = dr["S_Name"].ToString();
                    e_text.Text = dr["S_Enrollment_No"].ToString();
                    d_text.Text = dr["S_Development"].ToString();
                    se_text.Text = dr["S_Sem"].ToString();
                    c_text.Text = dr["S_Contact"].ToString();
                    em_text.Text = dr["S_Email"].ToString();


                }


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            wanted_path = Path.GetDirectoryName(Path.GetDirectoryName(System.IO.Directory.GetCurrentDirectory()));
            result = openFileDialog1.ShowDialog();
            openFileDialog1.Filter = "JPEG(*.jpeg)|*.jpeg|PNG Files(*.png)|*.png|JPG Files(*.jpg)|*.jpg|GIF Files(*.gif)|*.gif";
          
          
        }

        private void update_Click(object sender, EventArgs e)
        {
            int i;
            i = Convert.ToInt32(dataGridView1.SelectedCells[0].Value.ToString());
            if (result == DialogResult.OK) //Test Result
            {
                string img_path;
                File.Copy(openFileDialog1.FileName, wanted_path + "\\Student_Image\\" + pwd + ".jpg");
                img_path = "Student_Image\\" + pwd + ".jpg";
                
                SqlCommand cmd = con.CreateCommand();
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "update S_Info set S_Name='"+ s_text.Text+"',S_Image='"+img_path.ToString()+"',S_Enrollment_No='"+e_text.Text +"',S_Development='"+d_text.Text+"',S_Sem='"+se_text.Text+"',S_Contact='"+c_text.Text+"',S_Email='"+em_text.Text+"' where  Id=" + i + "";
                cmd.ExecuteNonQuery();
                fill_grid();

                s_text.Text = "";
                e_text.Text = "";
                d_text.Text = "";
                se_text.Text = "";
                c_text.Text = "";
                em_text.Text = "";

                MessageBox.Show("Updated!!!");
            }
            else if(result == DialogResult.Cancel)
            {
                MessageBox.Show("Cancel");
            }

        }
    }
}
