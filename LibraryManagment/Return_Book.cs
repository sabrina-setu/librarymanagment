﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace LibraryManagment
{
    public partial class Return_Book : Form
    {
        SqlConnection con = new SqlConnection(@"Data Source=DESKTOP-ADPGDPU;Initial Catalog=library_managment;Integrated Security=True");
        public Return_Book()
        {
            InitializeComponent();
        }

        private void sb_button_Click(object sender, EventArgs e)
        {
            panel2.Visible = true;
            fill_grid(e_textBox.Text);
        }

        private void Return_Book_Load(object sender, EventArgs e)
        {
            if(con.State== ConnectionState.Open)
            {
                con.Close();
            }
            con.Open();
        }
        public void fill_grid(string enrollment)
        {
            SqlCommand cmd = con.CreateCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "select *from Issues_Book where S_Enrollment='"+enrollment.ToString()+"' and Books_Return_Date=''";
            cmd.ExecuteNonQuery();
            DataTable dt = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            dataGridView1.DataSource = dt;
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            panel3.Visible = true;
            int i;
            i = Convert.ToInt32(dataGridView1.SelectedCells[0].Value.ToString());
            SqlCommand cmd = con.CreateCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "select *from Issues_Book where Id="+i+"";
            cmd.ExecuteNonQuery();
            DataTable dt = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            foreach(DataRow dr in dt.Rows)
            {
                lbl_bookName.Text = dr["Books_Name"].ToString();
                lbl_issuesDate.Text = dr["Books_Issues_Date"].ToString();
            }
        }

        private void r_button_Click(object sender, EventArgs e)
        {
            int i;
            i = Convert.ToInt32(dataGridView1.SelectedCells[0].Value.ToString());
            SqlCommand cmd = con.CreateCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "update Issues_Book set Books_Return_Date='"+ dateTimePicker1 .Value.ToString()+ "'  where Id=" + i + "";
            cmd.ExecuteNonQuery();

            SqlCommand cmd1 = con.CreateCommand();
            cmd1.CommandType = CommandType.Text;
            cmd1.CommandText = "update B_Info set Available_qty=Available_qty+1 where BookName='"+lbl_bookName.Text+"' ";
            cmd1.ExecuteNonQuery();

            MessageBox.Show("Return Sucessfully!!!");

            panel3.Visible = true;
            fill_grid(e_textBox.Text);
        }
    }
}
