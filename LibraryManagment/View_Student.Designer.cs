﻿namespace LibraryManagment
{
    partial class View_Student
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.S_textBox = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.update = new System.Windows.Forms.Button();
            this.em_text = new System.Windows.Forms.TextBox();
            this.se_text = new System.Windows.Forms.TextBox();
            this.c_text = new System.Windows.Forms.TextBox();
            this.d_text = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.e_text = new System.Windows.Forms.TextBox();
            this.s_text = new System.Windows.Forms.TextBox();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(236, 12);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(688, 196);
            this.dataGridView1.TabIndex = 0;
            this.dataGridView1.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellClick);
            // 
            // S_textBox
            // 
            this.S_textBox.Location = new System.Drawing.Point(30, 53);
            this.S_textBox.Name = "S_textBox";
            this.S_textBox.Size = new System.Drawing.Size(184, 20);
            this.S_textBox.TabIndex = 1;
            this.S_textBox.KeyUp += new System.Windows.Forms.KeyEventHandler(this.S_textBoxKeyUp);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.update);
            this.panel1.Controls.Add(this.em_text);
            this.panel1.Controls.Add(this.se_text);
            this.panel1.Controls.Add(this.c_text);
            this.panel1.Controls.Add(this.d_text);
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.e_text);
            this.panel1.Controls.Add(this.s_text);
            this.panel1.Location = new System.Drawing.Point(236, 234);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(688, 220);
            this.panel1.TabIndex = 2;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(20, 153);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(32, 13);
            this.label7.TabIndex = 14;
            this.label7.Text = "Email";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(20, 106);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(44, 13);
            this.label6.TabIndex = 13;
            this.label6.Text = "Contact";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(407, 107);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(53, 13);
            this.label5.TabIndex = 12;
            this.label5.Text = "Seamister";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(407, 63);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(62, 13);
            this.label4.TabIndex = 11;
            this.label4.Text = "Department";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(404, 25);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(73, 13);
            this.label3.TabIndex = 10;
            this.label3.Text = "Enrollment No";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(20, 64);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(36, 13);
            this.label2.TabIndex = 9;
            this.label2.Text = "Image";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(17, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "Name";
            // 
            // update
            // 
            this.update.Location = new System.Drawing.Point(546, 160);
            this.update.Name = "update";
            this.update.Size = new System.Drawing.Size(75, 23);
            this.update.TabIndex = 7;
            this.update.Text = "Update";
            this.update.UseVisualStyleBackColor = true;
            this.update.Click += new System.EventHandler(this.update_Click);
            // 
            // em_text
            // 
            this.em_text.Location = new System.Drawing.Point(89, 147);
            this.em_text.Name = "em_text";
            this.em_text.Size = new System.Drawing.Size(166, 20);
            this.em_text.TabIndex = 6;
            // 
            // se_text
            // 
            this.se_text.Location = new System.Drawing.Point(504, 106);
            this.se_text.Name = "se_text";
            this.se_text.Size = new System.Drawing.Size(157, 20);
            this.se_text.TabIndex = 5;
            // 
            // c_text
            // 
            this.c_text.Location = new System.Drawing.Point(89, 107);
            this.c_text.Name = "c_text";
            this.c_text.Size = new System.Drawing.Size(166, 20);
            this.c_text.TabIndex = 4;
            // 
            // d_text
            // 
            this.d_text.Location = new System.Drawing.Point(504, 66);
            this.d_text.Name = "d_text";
            this.d_text.Size = new System.Drawing.Size(157, 20);
            this.d_text.TabIndex = 3;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(89, 64);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(166, 23);
            this.button1.TabIndex = 2;
            this.button1.Text = "Select File";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // e_text
            // 
            this.e_text.Location = new System.Drawing.Point(504, 25);
            this.e_text.Name = "e_text";
            this.e_text.Size = new System.Drawing.Size(157, 20);
            this.e_text.TabIndex = 1;
            // 
            // s_text
            // 
            this.s_text.Location = new System.Drawing.Point(89, 25);
            this.s_text.Name = "s_text";
            this.s_text.Size = new System.Drawing.Size(166, 20);
            this.s_text.TabIndex = 0;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // View_Student
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(936, 479);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.S_textBox);
            this.Controls.Add(this.dataGridView1);
            this.Name = "View_Student";
            this.Text = "View Student Info";
            this.Load += new System.EventHandler(this.View_Student_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.TextBox S_textBox;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button update;
        private System.Windows.Forms.TextBox em_text;
        private System.Windows.Forms.TextBox se_text;
        private System.Windows.Forms.TextBox c_text;
        private System.Windows.Forms.TextBox d_text;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox e_text;
        private System.Windows.Forms.TextBox s_text;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
    }
}