﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
namespace LibraryManagment
{
    public partial class Issues_Book : Form
    {
        SqlConnection con = new SqlConnection(@"Data Source=DESKTOP-ADPGDPU;Initial Catalog=library_managment;Integrated Security=True");
        public Issues_Book()
        {
            InitializeComponent();
        }

        private void s_button_Click(object sender, EventArgs e)
        {
            int i=0;
            SqlCommand cmd = con.CreateCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "select * from S_Info where S_Enrollment_No='"+ e_textBox.Text+ "'";
            cmd.ExecuteNonQuery();
            DataTable dt = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            i = Convert.ToInt32(dt.Rows.Count.ToString());
            if (i == 0)
            {
                MessageBox.Show("Not Found!!!");
            }
            else
            {

                foreach (DataRow dr in dt.Rows)
                {
                    n_textBox.Text = dr["S_Name"].ToString();
                    d_textBox.Text = dr["S_Development"].ToString();
                    s_textBox.Text = dr["S_Sem"].ToString();
                    c_textBox.Text = dr["S_Contact"].ToString();
                    em_textBox.Text = dr["S_Email"].ToString();
                }
            }
        }

        private void Issues_Book_Load(object sender, EventArgs e)
        {
            if(con.State==ConnectionState.Open)
            {
                con.Close();
            }
            con.Open();
        }

        private void e_textBox_KeyUp(object sender, KeyEventArgs e)
        {
             
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void bn_textBox_KeyUp(object sender, KeyEventArgs e)
        {
            int count = 0;
            if (e.KeyCode != Keys.Enter)
            {
                listBox1.Items.Clear();
                SqlCommand cmd = con.CreateCommand();
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "select * from B_Info where BookName like('%" + bn_textBox.Text + "%')";
                cmd.ExecuteNonQuery();
                DataTable dt = new DataTable();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
                count = Convert.ToInt32(dt.Rows.Count.ToString());
                if (count > 0)
                {
                    listBox1.Visible = true;
                    foreach (DataRow dr in dt.Rows)
                    {
                        listBox1.Items.Add(dr["BookName "].ToString());
                    }
                }
            }
        }

        private void bn_textBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Down)
            {
                listBox1.Focus();
                //you might need to select one value to allow arrow key
                listBox1.SelectedIndex=0;

            }
        }

        private void listBox1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Down)
            {
                bn_textBox.Text = listBox1.SelectedItem.ToString();
                listBox1.Visible = false;
            }
        }

        private void listBox1_MouseClick(object sender, MouseEventArgs e)
        {
            bn_textBox.Text = listBox1.SelectedItem.ToString();
            listBox1.Visible = false;

        }

        private void i_button_Click(object sender, EventArgs e)
        {
            int book_qty = 0;
            SqlCommand cmd2 = con.CreateCommand();
            cmd2.CommandType = CommandType.Text;
            cmd2.CommandText = "select *from B_Info where BookName='"+bn_textBox.Text+"'";
            cmd2.ExecuteNonQuery();
            DataTable dt2 = new DataTable();
            SqlDataAdapter da2 = new SqlDataAdapter(cmd2);
            da2.Fill(dt2);
            foreach(DataRow dr2 in dt2.Rows)
            {
                book_qty = Convert.ToInt32(dr2["available_qty"].ToString());
            }
            if (book_qty > 0)
            {

                SqlCommand cmd = con.CreateCommand();
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "insert into Issues_Book(S_Enrollment,S_Name,S_Dept,S_Sem,S_Contact,S_Email,Books_Name,Books_Issues_Date,Books_Return_Date) values('" + e_textBox.Text + "','" + n_textBox.Text + "','" + d_textBox.Text + "','" + s_textBox.Text + "','" + c_textBox.Text + "','" + em_textBox.Text + "','" + bn_textBox.Text + "','" + dateTimePicker1.Value.ToString() + "','')";
                cmd.ExecuteNonQuery();

                SqlCommand cmd1 = con.CreateCommand();
                cmd1.CommandType = CommandType.Text;
                cmd1.CommandText = "Update B_Info set Available_qty=Available_qty-1 where BookName='" + bn_textBox.Text + "'";
                cmd1.ExecuteNonQuery();

                n_textBox.Text = "";
                d_textBox.Text = "";
                s_textBox.Text = "";
                c_textBox.Text = "";
                em_textBox.Text = "";
                bn_textBox.Text = "";
                e_textBox.Text = "";
                MessageBox.Show("Issues Sucessfully!!!!");
            }
            else
            {
                MessageBox.Show("Book is Not Available!!!!");
            }
        }
    }
}
