﻿namespace LibraryManagment
{
    partial class Add_Books
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.n_textBox = new System.Windows.Forms.TextBox();
            this.a_textBox = new System.Windows.Forms.TextBox();
            this.p_textBox = new System.Windows.Forms.TextBox();
            this.pr_textBox = new System.Windows.Forms.TextBox();
            this.q_textBox = new System.Windows.Forms.TextBox();
            this.save = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.SuspendLayout();
            // 
            // n_textBox
            // 
            this.n_textBox.Location = new System.Drawing.Point(294, 27);
            this.n_textBox.Name = "n_textBox";
            this.n_textBox.Size = new System.Drawing.Size(223, 20);
            this.n_textBox.TabIndex = 0;
            // 
            // a_textBox
            // 
            this.a_textBox.Location = new System.Drawing.Point(294, 69);
            this.a_textBox.Name = "a_textBox";
            this.a_textBox.Size = new System.Drawing.Size(223, 20);
            this.a_textBox.TabIndex = 1;
            // 
            // p_textBox
            // 
            this.p_textBox.Location = new System.Drawing.Point(294, 111);
            this.p_textBox.Name = "p_textBox";
            this.p_textBox.Size = new System.Drawing.Size(223, 20);
            this.p_textBox.TabIndex = 2;
            // 
            // pr_textBox
            // 
            this.pr_textBox.Location = new System.Drawing.Point(294, 200);
            this.pr_textBox.Name = "pr_textBox";
            this.pr_textBox.Size = new System.Drawing.Size(223, 20);
            this.pr_textBox.TabIndex = 4;
            // 
            // q_textBox
            // 
            this.q_textBox.Location = new System.Drawing.Point(294, 243);
            this.q_textBox.Name = "q_textBox";
            this.q_textBox.Size = new System.Drawing.Size(223, 20);
            this.q_textBox.TabIndex = 5;
            // 
            // save
            // 
            this.save.Location = new System.Drawing.Point(442, 304);
            this.save.Name = "save";
            this.save.Size = new System.Drawing.Size(75, 23);
            this.save.TabIndex = 6;
            this.save.Text = "Save";
            this.save.UseVisualStyleBackColor = true;
            this.save.Click += new System.EventHandler(this.save_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(201, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "Name";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(204, 75);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(38, 13);
            this.label2.TabIndex = 8;
            this.label2.Text = "Author";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(204, 111);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(62, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "Publication ";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(204, 157);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(72, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "Purchas Date";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(207, 200);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(31, 13);
            this.label5.TabIndex = 11;
            this.label5.Text = "Price";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(207, 243);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(46, 13);
            this.label6.TabIndex = 12;
            this.label6.Text = "Quantity";
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.CustomFormat = "dd.mm.yyyy";
            this.dateTimePicker1.Location = new System.Drawing.Point(294, 157);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(223, 20);
            this.dateTimePicker1.TabIndex = 13;
            this.dateTimePicker1.Value = new System.DateTime(2019, 1, 16, 0, 0, 0, 0);
            // 
            // Add_Books
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(621, 402);
            this.Controls.Add(this.dateTimePicker1);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.save);
            this.Controls.Add(this.q_textBox);
            this.Controls.Add(this.pr_textBox);
            this.Controls.Add(this.p_textBox);
            this.Controls.Add(this.a_textBox);
            this.Controls.Add(this.n_textBox);
            this.Name = "Add_Books";
            this.Text = "Add Books";
            this.Load += new System.EventHandler(this.Add_Books_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox n_textBox;
        private System.Windows.Forms.TextBox a_textBox;
        private System.Windows.Forms.TextBox p_textBox;
        private System.Windows.Forms.TextBox pr_textBox;
        private System.Windows.Forms.TextBox q_textBox;
        private System.Windows.Forms.Button save;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
    }
}