﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Data.SqlClient;

namespace LibraryManagment
{
    public partial class Add_Student_Info : Form
    {
        SqlConnection con = new SqlConnection(@"Data Source=DESKTOP-ADPGDPU;Initial Catalog=library_managment;Integrated Security=True");
        //string pwd;
        string wanted_path;
        string pwd = Class1.GetRandomPassword(20);
        public Add_Student_Info()
        {
            InitializeComponent();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            
            wanted_path = Path.GetDirectoryName(Path.GetDirectoryName(System.IO.Directory.GetCurrentDirectory()));
            DialogResult result = openFileDialog1.ShowDialog();
            openFileDialog1.Filter = "JPEG(*.jpeg)|*.jpeg|PNG Files(*.png)|*.png|JPG Files(*.jpg)|*.jpg|GIF Files(*.gif)|*.gif";
            if(result==DialogResult.OK) //Test Result
            {
                pictureBox1.ImageLocation = openFileDialog1.FileName;
                pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
            }
            //pictureBox1.ImageLocation=@"..\..\Student_Image"+pwd+".jpg";
        }

        private void Save_Click(object sender, EventArgs e)
        {
            try
            {
                string img_path;
                File.Copy(openFileDialog1.FileName, wanted_path + "\\Student_Image\\" + pwd + ".jpg");
                img_path = "Student_Image\\" + pwd + ".jpg";
                con.Open();
                SqlCommand cmd = con.CreateCommand();
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "insert into S_Info values ('" + S_text.Text + "','" + img_path.ToString() + "','" + E_text.Text + "','" + D_text.Text + "','" + Se_text.Text + "','" + C_text.Text + "','" + Em_text.Text + "')";
                cmd.ExecuteNonQuery();
                con.Close();

                S_text.Text = "";
                E_text.Text = "";
                D_text.Text = "";
                Se_text.Text = "";
                C_text.Text = "";
                Em_text.Text = "";

                MessageBox.Show("Inserted Sucessfully!!");
            }

            catch(Exception ex)
            {
                MessageBox.Show(ex.Message.ToString());
            }
        }
    }
}
