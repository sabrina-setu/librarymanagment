﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace LibraryManagment
{
    public partial class Report_Books_Remain_Retain : Form
    {
        SqlConnection con = new SqlConnection(@"Data Source=DESKTOP-ADPGDPU;Initial Catalog=library_managment;Integrated Security=True");
        public Report_Books_Remain_Retain()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DataSet1 ds = new DataSet1();
            SqlCommand cmd = con.CreateCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "select *from Issues_Book where Books_Return_Date=''";
            cmd.ExecuteNonQuery();
            DataTable dt = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(ds.DataTable1);
            CrystalReport1 myReport = new CrystalReport1();
            myReport.SetDataSource(ds);
            crystalReportViewer1.ReportSource = myReport;

        }

        private void Report_Books_Remain_Retain_Load(object sender, EventArgs e)
        {
            if(con.State==ConnectionState.Open)
            {
                con.Close();
            }
            con.Open();
        }
    }
}
