﻿namespace LibraryManagment
{
    partial class View_Books
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.S_textBox = new System.Windows.Forms.TextBox();
            this.search_button = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.a_Go = new System.Windows.Forms.Button();
            this.author_Text = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.update = new System.Windows.Forms.Button();
            this.dateTimePicker2 = new System.Windows.Forms.DateTimePicker();
            this.Q_textBox = new System.Windows.Forms.TextBox();
            this.P_textBox = new System.Windows.Forms.TextBox();
            this.Pn_textBox = new System.Windows.Forms.TextBox();
            this.An_textBox = new System.Windows.Forms.TextBox();
            this.TextBox = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(130, 128);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(649, 147);
            this.dataGridView1.TabIndex = 0;
            this.dataGridView1.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellClick);
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(22, 33);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(76, 24);
            this.label1.TabIndex = 1;
            this.label1.Text = "Search";
            // 
            // S_textBox
            // 
            this.S_textBox.Location = new System.Drawing.Point(114, 38);
            this.S_textBox.Name = "S_textBox";
            this.S_textBox.Size = new System.Drawing.Size(187, 20);
            this.S_textBox.TabIndex = 2;
            this.S_textBox.KeyUp += new System.Windows.Forms.KeyEventHandler(this.S_textBox_KeyUp);
            // 
            // search_button
            // 
            this.search_button.Location = new System.Drawing.Point(226, 64);
            this.search_button.Name = "search_button";
            this.search_button.Size = new System.Drawing.Size(75, 23);
            this.search_button.TabIndex = 3;
            this.search_button.Text = "Go";
            this.search_button.UseVisualStyleBackColor = true;
            this.search_button.Click += new System.EventHandler(this.search_button_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.search_button);
            this.panel1.Controls.Add(this.S_textBox);
            this.panel1.Location = new System.Drawing.Point(130, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(330, 100);
            this.panel1.TabIndex = 4;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.a_Go);
            this.panel2.Controls.Add(this.author_Text);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Location = new System.Drawing.Point(475, 12);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(304, 100);
            this.panel2.TabIndex = 5;
            // 
            // a_Go
            // 
            this.a_Go.Location = new System.Drawing.Point(199, 63);
            this.a_Go.Name = "a_Go";
            this.a_Go.Size = new System.Drawing.Size(75, 23);
            this.a_Go.TabIndex = 2;
            this.a_Go.Text = "Go";
            this.a_Go.UseVisualStyleBackColor = true;
            this.a_Go.Click += new System.EventHandler(this.a_Go_Click);
            // 
            // author_Text
            // 
            this.author_Text.Location = new System.Drawing.Point(130, 32);
            this.author_Text.Name = "author_Text";
            this.author_Text.Size = new System.Drawing.Size(168, 20);
            this.author_Text.TabIndex = 1;
            this.author_Text.KeyUp += new System.Windows.Forms.KeyEventHandler(this.author_Text_KeyUp);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(19, 32);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(105, 16);
            this.label2.TabIndex = 0;
            this.label2.Text = "Search Author";
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.update);
            this.panel3.Controls.Add(this.dateTimePicker2);
            this.panel3.Controls.Add(this.Q_textBox);
            this.panel3.Controls.Add(this.P_textBox);
            this.panel3.Controls.Add(this.Pn_textBox);
            this.panel3.Controls.Add(this.An_textBox);
            this.panel3.Controls.Add(this.TextBox);
            this.panel3.Controls.Add(this.label8);
            this.panel3.Controls.Add(this.label7);
            this.panel3.Controls.Add(this.label6);
            this.panel3.Controls.Add(this.label5);
            this.panel3.Controls.Add(this.label4);
            this.panel3.Controls.Add(this.label3);
            this.panel3.Location = new System.Drawing.Point(130, 293);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(649, 161);
            this.panel3.TabIndex = 6;
            this.panel3.Visible = false;
            // 
            // update
            // 
            this.update.Location = new System.Drawing.Point(285, 124);
            this.update.Name = "update";
            this.update.Size = new System.Drawing.Size(79, 34);
            this.update.TabIndex = 13;
            this.update.Text = "Update";
            this.update.UseVisualStyleBackColor = true;
            this.update.Click += new System.EventHandler(this.update_Click);
            // 
            // dateTimePicker2
            // 
            this.dateTimePicker2.Location = new System.Drawing.Point(424, 19);
            this.dateTimePicker2.Name = "dateTimePicker2";
            this.dateTimePicker2.Size = new System.Drawing.Size(200, 20);
            this.dateTimePicker2.TabIndex = 12;
            // 
            // Q_textBox
            // 
            this.Q_textBox.Location = new System.Drawing.Point(424, 92);
            this.Q_textBox.Name = "Q_textBox";
            this.Q_textBox.Size = new System.Drawing.Size(195, 20);
            this.Q_textBox.TabIndex = 11;
            // 
            // P_textBox
            // 
            this.P_textBox.Location = new System.Drawing.Point(424, 50);
            this.P_textBox.Name = "P_textBox";
            this.P_textBox.Size = new System.Drawing.Size(195, 20);
            this.P_textBox.TabIndex = 10;
            this.P_textBox.TextChanged += new System.EventHandler(this.P_textBox_TextChanged);
            // 
            // Pn_textBox
            // 
            this.Pn_textBox.Location = new System.Drawing.Point(70, 92);
            this.Pn_textBox.Name = "Pn_textBox";
            this.Pn_textBox.Size = new System.Drawing.Size(209, 20);
            this.Pn_textBox.TabIndex = 8;
            // 
            // An_textBox
            // 
            this.An_textBox.Location = new System.Drawing.Point(70, 56);
            this.An_textBox.Name = "An_textBox";
            this.An_textBox.Size = new System.Drawing.Size(209, 20);
            this.An_textBox.TabIndex = 7;
            // 
            // TextBox
            // 
            this.TextBox.Location = new System.Drawing.Point(70, 19);
            this.TextBox.Name = "TextBox";
            this.TextBox.Size = new System.Drawing.Size(209, 20);
            this.TextBox.TabIndex = 6;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(367, 99);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(46, 13);
            this.label8.TabIndex = 5;
            this.label8.Text = "Quantity";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(367, 50);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(31, 13);
            this.label7.TabIndex = 4;
            this.label7.Text = "Price";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(346, 22);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(72, 13);
            this.label6.TabIndex = 3;
            this.label6.Text = "Purchas Date";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(5, 95);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(59, 13);
            this.label5.TabIndex = 2;
            this.label5.Text = "Publication";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(20, 57);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(38, 13);
            this.label4.TabIndex = 1;
            this.label4.Text = "Author";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(5, 22);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(63, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "Book Name";
            // 
            // View_Books
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(968, 466);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.dataGridView1);
            this.Name = "View_Books";
            this.Text = "View Books";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.View_Books_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox S_textBox;
        private System.Windows.Forms.Button search_button;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button a_Go;
        private System.Windows.Forms.TextBox author_Text;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button update;
        private System.Windows.Forms.DateTimePicker dateTimePicker2;
        private System.Windows.Forms.TextBox Q_textBox;
        private System.Windows.Forms.TextBox P_textBox;
        private System.Windows.Forms.TextBox Pn_textBox;
        private System.Windows.Forms.TextBox An_textBox;
        private System.Windows.Forms.TextBox TextBox;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
    }
}