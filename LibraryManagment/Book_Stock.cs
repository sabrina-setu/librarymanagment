﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Net.Mail;
using System.Net;

namespace LibraryManagment
{
    public partial class Book_Stock : Form
    {
        SqlConnection con = new SqlConnection(@"Data Source=DESKTOP-ADPGDPU;Initial Catalog=library_managment;Integrated Security=True");
        public Book_Stock()
        {
            InitializeComponent();
        }

        private void Book_Stock_Load(object sender, EventArgs e)
        {
            if(con.State==ConnectionState.Open)
            {
                con.Close();
            }
            con.Open();
            fill_books_info();
        }
        public void fill_books_info()
        {
            SqlCommand cmd = con.CreateCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "select BookName,AuthorName,Quantity,Available_qty from B_Info";
            cmd.ExecuteNonQuery();
            DataTable dt = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            dataGridView1.DataSource = dt;
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            string i;
            i = dataGridView1.SelectedCells[0].Value.ToString();
            SqlCommand cmd = con.CreateCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "select *from Issues_Book where Books_Name='"+i.ToString()+"' and Books_Return_Date=''";
            cmd.ExecuteNonQuery();
            DataTable dt = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            dataGridView2.DataSource = dt;
        }

        private void textBox1_KeyUp(object sender, KeyEventArgs e)
        {
            SqlCommand cmd = con.CreateCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "select BookName,AuthorName,Quantity,Available_qty from B_Info where BookName like('%"+ textBox1.Text+"%')";
            cmd.ExecuteNonQuery();
            DataTable dt = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            dataGridView1.DataSource = dt;
        }

        private void dataGridView2_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            string i;
            i = dataGridView2.SelectedCells[6].Value.ToString();
            e_textBox.Text = i.ToString();
        }

        private void s_button_Click(object sender, EventArgs e)
        {
            //SmtpClient smtp = new SmtpClient("smtp.gmail.com", 578);
            //smtp.EnableSsl = true;
            //smtp.UseDefaultCredentials = false;
            ////(Username,Password)
            //smtp.Credentials = new NetworkCredential("xxxxx@gmail.com", "xxxxx");
            ////(from,to,subject,body)
            //MailMessage mail = new MailMessage("xxxxx@gmail.com", e_textBox.Text, "This is for book return notice.", c_textBox.Text);
            //mail.Priority = MailPriority.High;
            //smtp.Send(mail);
            //MessageBox.Show("Mail Send!!!");
            try
            {
                MailMessage message = new MailMessage();
                SmtpClient smtp = new SmtpClient();

                message.From = new MailAddress("from@gmail.com");
                message.To.Add(new MailAddress("to@gmail.com"));
                message.Subject = "Test";
                message.Body = "Content";

                smtp.Port = 587;
                smtp.Host = "smtp.gmail.com";
                smtp.EnableSsl = true;
                smtp.UseDefaultCredentials = false;
                smtp.Credentials = new NetworkCredential("from@gmail.com", "pwd");
                smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                smtp.Send(message);
                MessageBox.Show("Mail Send!!");
            }
            catch (Exception ex)
            {
                MessageBox.Show("err: " + ex.Message);
            }

        }
    }
}
